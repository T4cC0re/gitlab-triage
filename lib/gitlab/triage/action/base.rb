module Gitlab
  module Triage
    module Action
      class Base
        attr_reader :policy, :network

        def initialize(policy:, network:)
          @policy = policy
          @network = network
        end
      end
    end
  end
end
