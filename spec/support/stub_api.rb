module StubAPI
  def stub_api(method, url, query: {}, body: {}, headers: {})
    response =
      if block_given?
        yield
      else
        {}
      end

    content_type = { 'Content-Type' => 'application/json' }

    WebMock::API.stub_request(method, url)
      .with(query: query, body: body, headers: content_type.merge(headers))
      .to_return(
        body: JSON.dump(response),
        headers: content_type.merge('X-Next-Page' => ''))
  end

  def stub_delete(url)
    WebMock::API.stub_request(:delete, url)
        .to_return(status: 200)
  end
end
